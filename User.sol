pragma solidity^0.6.0;
import "./IUser.sol"; //导入另外一个文件

contract User is IUser {
    string name;
    uint256 age;
    
    function setName(string calldata _name) override external {
        name = _name;
    }
    function getName()override external view returns (string memory) {
        return name;
    }
    function setAge(uint256 _age) override external {
        age = _age;
    }
    function getAge() override external view returns (uint256) {
        return age;
    }
}
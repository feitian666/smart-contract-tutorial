pragma solidity^0.6.0;

contract hello {
    string hellomsg;
    
    constructor(string memory _msg) public {
        hellomsg = _msg;
    }
    
    function getmsg() public view returns (string memory) {
        return hellomsg;
    }
}
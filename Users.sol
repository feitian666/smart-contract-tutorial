pragma solidity^0.6.0;
import "./User.sol";

contract Users {
    IUser user; // 通过合约接口定义了合约对象
    
    
    constructor() public {
        user = new User();//创建一个User对象
    }
    
    function upgrade(address addr) public {
        user = User(addr);
    }
    
    function getName() public view returns (string memory) {
        return user.getName();
    }
    
    function setName(string memory _name) public {
        user.setName(_name);
    }
}
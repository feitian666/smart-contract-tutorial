pragma solidity^0.6.0;

contract modifiers {
    address owner;
    
    constructor() public {
        owner = msg.sender;// msg.sender 合约的调用者，它是一个地址
    }
    
    function showOwner() external view returns (address) {
        return owner;
    }
    
    //判断一下是否是owner用户在调用
    function isOwner() internal view returns (bool) {
        return owner == msg.sender;
    }
    
    function showAdd(uint256 a, uint256 b) private pure returns (uint256) {
        return a + b;
    }
    
    function showMul(uint256 a, uint256 b) public view returns (uint256) {
        if(isOwner()) {
            return a * b;
        } 
        //showOwner(); //不能调用extetnal函数
        return 0;
    }
    //携带payable的函数
    function daqian() public payable {
        //do nothing
    }
    
}
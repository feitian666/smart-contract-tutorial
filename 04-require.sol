pragma solidity^0.6.0;


contract requiredemo {
    
    uint256 age ;
    
    constructor() public {
        age = 40;
    }
    
    function testAge() public  {
        require(age == 40, "age must equal 40"); //当条件不满足时，将会触发断言错误
        age = 40 + 1;
    }
    
    function updAge() public {
        assert(age == 40);//当条件不满足时，将会触发断言错误,扣光gas
        age += 1;
    }
}
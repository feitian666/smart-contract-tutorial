pragma solidity^0.6.0;

contract modifierdemo {
    address admin;
    address owner;
    uint256 count;
    
    constructor(address _owner) public {
        admin = msg.sender;
        owner = _owner;
        count = 100 * 10000;
    }
    
    modifier onlyadmin() {
        require(msg.sender == admin, "only admin can do");
        _;
    }
    
    modifier onlyowner() {
        require(msg.sender == owner, "only owner can do");
        _;
    }
    
    function setCount() public onlyowner {
        count *= 2;
    }
    
    function getCount() public view returns (uint256) {
        return count;
    }
}
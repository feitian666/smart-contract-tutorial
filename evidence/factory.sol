pragma solidity ^0.8.7;
import "./evidence.sol";

contract EvidenceFactory is IEvidence {
   address[] signers;
        
   //advance to add logic about free key
   mapping(string=>address) evi_key; 
        
   event newEvidenceEvent(address addr);
   event newEvidenceEventWithKey(address addr, string key);
   
   constructor(address[] memory evidenceSigners){
      for(uint i=0; i<evidenceSigners.length; ++i) {
            signers.push(evidenceSigners[i]);
      }
   }

   // 创建存证
   function newEvidence(string memory evi) public returns(address)
   {
         Evidence evidence = new Evidence(evi, address(this));
         emit newEvidenceEvent(address(evidence));
         return address(evidence);
   }

   // 通过key记录存证
   function newEvidenceByKey(string memory evi, string memory key) public returns(address)
   {
      Evidence evidence = new Evidence(evi, address(this));
      emit newEvidenceEventWithKey(address(evidence), key); 
      evi_key[key] = address(evidence);
      return address(evidence);
   }
        
   function getEvidenceByKey(string memory key) public view returns(string memory,address[] memory,address[] memory){ 
      return getEvidence(evi_key[key]);
   }
   
   function getEvidence(address addr) public view returns(string memory,address[] memory,address[] memory){
      return Evidence(addr).getEvidence();
   }

            
   function addSignatures(address addr) public returns(bool) {
      return Evidence(addr).addSignatures();
   }
        
   

   function verify(address addr) override public view returns(bool){
      for(uint i=0; i<signers.length; ++i) {
            if (addr == signers[i])
            {
               return true;
            }
      }
      return false;
   }

   function getSigner(uint index) override public view returns(address){
      uint listSize = signers.length;
      if(index < listSize)
      {
            return signers[index];
      }
      else
      {
            return address(0);
      }

   }

   function getSignersSize() override public view returns(uint){
      return signers.length;
   }

   function getSigners() public view returns(address[] memory){
      return signers;
   }

}
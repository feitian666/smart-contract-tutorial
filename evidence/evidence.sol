pragma solidity ^0.8.7;

interface IEvidence
{
	function verify(address addr) external view returns(bool);
	function getSigner(uint index) external view returns(address);
	function getSignersSize() external view returns(uint);
}

contract Evidence {
    
    string evidence;
    address[] signers;
    address public factoryAddr;
    
    event addSignaturesEvent(string evi);
    event newSignaturesEvent(string evi, address addr);
    event errorNewSignaturesEvent(string evi, address addr);
    event errorAddSignaturesEvent(string evi, address addr);
    event addRepeatSignaturesEvent(string evi);
    event errorRepeatSignaturesEvent(string evi, address addr);

    function CallVerify(address addr) public view returns(bool) {
        return IEvidence(factoryAddr).verify(addr);
    }

   constructor(string memory evi, address addr)  {
       factoryAddr = addr;
       if(CallVerify(tx.origin))
       {
           evidence = evi;
           signers.push(tx.origin);
           emit newSignaturesEvent(evi,addr);
       }
       else
       {
           emit errorNewSignaturesEvent(evi,addr);
       }
    }

    function getEvidence() public view returns(string memory,address[] memory,address[] memory){
        uint length = IEvidence(factoryAddr).getSignersSize();
         address[] memory signerList = new address[](length);
         for(uint i= 0 ;i<length ;i++)
         {
             signerList[i] = (IEvidence(factoryAddr).getSigner(i));
         }
        return(evidence,signerList,signers);
    }

    function addSignatures() public returns(bool) {
        for(uint i= 0 ;i<signers.length ;i++)
        {
            if(tx.origin == signers[i])
            {
               emit  addRepeatSignaturesEvent(evidence);
               return true;
            }
        }
       if(CallVerify(tx.origin))
       {
            signers.push(tx.origin);
            emit addSignaturesEvent(evidence);
            return true;
       }
       else
       {
           emit errorAddSignaturesEvent(evidence,tx.origin);
           return false;
       }
    }
    
    function getSigners()public view returns(address[] memory)
    {
         uint length = IEvidence(factoryAddr).getSignersSize();
         address[] memory signerList = new address[](length);
         for(uint i= 0 ;i<length ;i++)
         {
             signerList[i] = (IEvidence(factoryAddr).getSigner(i));
         }
         return signerList;
    }
}
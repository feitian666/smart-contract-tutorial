pragma solidity^0.6.0;

//定义接口
interface IUser {
    
    function setName(string calldata _name) external ;
    function getName() external view returns (string memory);
    function setAge(uint256 _age) external;
    function getAge() external view returns (uint256);
}
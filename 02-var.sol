pragma solidity^0.6.0;


contract vardemo {
    string public author; // public 类型的变量会提供一个查询方法
    string public sex;
    uint256 sal;
    bytes32 public hash;
    
    constructor() public {
        author = "yekai";
        sex = "man";
        sal = 1000000;
        hash = keccak256(abi.encode(author, sex, sal));
    }
    
    function getSal() public view returns (uint256) {
        return sal;
    }
    
    function getInfo() public view returns (string memory, string memory, uint256, bytes32) {
        return (author, sex, sal, hash);
    }
}